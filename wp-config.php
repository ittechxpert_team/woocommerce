<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'woocommerce' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '8]PT8M&6T`m(?>`/E:tS;L-q.B.Q /E&L~wq=GIAaP^omk[<$|(!@o@x]i+}U+i{' );
define( 'SECURE_AUTH_KEY',  'P5Z&m5E>/y ~tY&fSL5r~8O>nshP<o<Sp{7;4R!i[:FRRa+]`&gmHa#VG:yx02V^' );
define( 'LOGGED_IN_KEY',    's3e>Za(AE%<3t!.k~9cqG{,2wwIrNnT-%8cbvQiOAm.0=7}WBS~z1Q%-mr4ClVfo' );
define( 'NONCE_KEY',        'IMfnOnG~!08)BGSt@<kKvxopnNhvv#o,$!2F}CxN7)8ixW}DN.WKNB?dJByzErmq' );
define( 'AUTH_SALT',        'u?Qi_^Av#aSl_#yI4E/ZbR]1]G?:.N)6wIDet3yXuYi~^p<Td&36fUc#-I#kio$_' );
define( 'SECURE_AUTH_SALT', 'V_$p`{Q0y<y-`A>XhwgkJxHnYgp]Tbgz:Q4Ux5lqZyC0ww7pBoq7hxrcsbSxG]wT' );
define( 'LOGGED_IN_SALT',   '8hifPwZBipT~H]mN}ASaOqC_eFaqDu:#4%MbaFYyX<_/fC$&os{VmnUt&UyR@?v{' );
define( 'NONCE_SALT',       'W-b<Dz9M.KKdvxXf&tAiZ UjX`n._|mu)YLatT GQRR]_7~p&,syjx:&o2+H?=V~' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
